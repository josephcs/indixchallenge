from flask import Flask, request, Response, render_template

import json
import requests
import re

app = Flask(__name__)

@app.route('/')
def index():
    r = requests.get('http://' + request.environ['SERVER_NAME'] + ':5000')
    response_json = r.json()
    competitors = response_json['competitors']
    data = response_json['data']
    return render_template('index.html', data=data, competitors=competitors)

@app.context_processor
def utility_processor():
    def format_field(category, store):
        concat = category + '_' + store
        return re.sub('[^0-9a-zA-Z]+', '_', concat).lower()
    return dict(format_field=format_field)

if __name__ == "__main__":
    app.run(port=5001)