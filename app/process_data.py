import json
import os
import pandas


SOURCE_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'sampledata.csv')

class DataHandler:
    def __init__(self, source_file=SOURCE_FILE, store_name="My Store"):
        self.source_file = source_file
        self.store_name = store_name

    def _get_competitors(self, pd):
        competitor_object = pandas.unique(pd.Store)
        competitors = list(competitor_object)
        competitors.remove(self.store_name)
        compeitors = competitors.sort()
        return competitors

    def _build_mean(self, group):
        data = {}
        store_data = {}
        my_store = {}
        competitors = []
        for index, row in group.iterrows():
            print index
            category = index[0]
            competitor = index[1]
            if category not in store_data:
                store_data[category] = []
            if competitor == self.store_name:
                my_store[category] = {'average': round(float(row), 3)}
                continue
            store_data[category].append({'store': competitor, 'average': round(float(row), 3)})
            data['competitor_mean'] = store_data
            data['store_mean'] = my_store
        return data

    def execute(self):
        print 'Reading the file'
        pd = pandas.read_csv(self.source_file)
        pd.index = pd['Top Level Category']
        print 'Processing'
        group = pd.groupby(['Top Level Category', 'Store']).mean()
        data = self._build_mean(group)
        data['competitors'] = self._get_competitors(pd)
        file_loc = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data.json')
        with open(file_loc, 'w') as data_file:
            json.dump(data, data_file, indent=2)

DataHandler().execute()