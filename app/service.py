from flask import Flask, request, Response, render_template

import json
import os
import requests

app = Flask(__name__)
SOURCE_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'sampledata.csv')

@app.route('/')
def index():
    data = {}
    file_loc = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data.json')
    with open(file_loc) as data_file:
        data = json.load(data_file)
    my_store = data.pop('store_mean')
    response_json = {}
    response_json['data'] = []
    categories = sorted(my_store.keys())
    for category in categories:
        response_json['data'].append({
            'name': category,
            'average': my_store[category]['average'],
            'store_data': data['competitor_mean'][category]
        })
    response_json['competitors'] = data['competitors']
    response_json = json.dumps(response_json)
    return Response(response=response_json, status=200, mimetype="application/json")

if __name__ == "__main__":
    app.run(port=5000)